

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 * Main class for Pirex; constructs GUI.
 * 
 * @author Group K
 */
public class Main
{

  /**
   * Main method for Pirex. Puts out the GUI.
   * 
   * @param args
   *          that might be passed into the program
   */
  public static void main(String[] args)
  {

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("GP3 - The Nortons");
    JTabbedPane tabbedPane = new JTabbedPane();

    ImageIcon searchIcon = new ImageIcon("resources/search.png");
    SearchWindow search = new SearchWindow();
    tabbedPane.addTab("1st Tab", searchIcon, search, "Searches for Books");

    ImageIcon loadIcon = new ImageIcon("resources/load.png");
    SummaryWindow summary = new SummaryWindow();
    LoadWindow load = new LoadWindow(summary);
    tabbedPane.addTab("2nd Tab", loadIcon, load, "Load Books into Database");

    ImageIcon databaseIcon = new ImageIcon("resources/library.png");
    // JComponent panel3 = new SummaryWindow();
    tabbedPane.addTab("3rd Tab", databaseIcon, summary, "Summary Form of Book Database");
    
    Tab4 four = new Tab4();
    tabbedPane.addTab("4th Tab", databaseIcon, four, "Final Tab");

    frame.setPreferredSize(new Dimension(1000, 800));
    frame.add(tabbedPane);
    frame.pack();
    frame.setVisible(true);

  }
}
