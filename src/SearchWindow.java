

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * JPanel extended class for the Pirex Search Window.
 * 
 * @author Group K
 */
public class SearchWindow extends JPanel
{
  public static final long serialVersionUID = 1L;
  private JPanel topButtons, highTermSearch, lowTermSearch, paragraphsPanel;
  private JRadioButton andRadio, orRadio;
  private JLabel radioText, highWeight, lowWeight, paragraphsFound;
  private JButton clear;
  private JList<String> shortForm;
  private ButtonGroup radioButtons;
  private JTextField highTerms, lowTerms;
  private JTextPane longForm;
  private JScrollPane shortScroll, longScroll;

  /**
   * Constructs the Pirex Search Window for display in main().
   */
  public SearchWindow()
  {
    // call parent and set layout
    super();
    this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    radioText = new JLabel("Combine Terms Using:");

    // create and add the radio buttons and the clear button
    topButtons = new JPanel();
    topButtons.setLayout(new BoxLayout(topButtons, BoxLayout.LINE_AXIS));

    String and = new String("And");
    String or = new String("Or");

    // can be accessed by pressing A plus Alt (or whatever the users look-and-feel is)
    andRadio = new JRadioButton(and);
    andRadio.setMnemonic(KeyEvent.VK_A);

    // can be accessed by pressing O plus Alt
    orRadio = new JRadioButton(or);
    orRadio.setMnemonic(KeyEvent.VK_O);

    // associate the buttons
    radioButtons = new ButtonGroup();
    radioButtons.add(orRadio);
    radioButtons.add(andRadio);
    orRadio.setSelected(true);

    // create clear button
    clear = new JButton("Clear All Terms");

    // add everything to our top panel
    topButtons.add(radioText);
    topButtons.add(orRadio);
    topButtons.add(andRadio);
    topButtons.add(Box.createHorizontalGlue());
    topButtons.add(clear);

    // add text fields for search terms
    highTermSearch = new JPanel();
    highTermSearch.setLayout(new BoxLayout(highTermSearch, BoxLayout.LINE_AXIS));
    highWeight = new JLabel("High Weight Search Terms:");
    highTerms = new JTextField();
    highTerms.setMaximumSize(new Dimension(Integer.MAX_VALUE, highTerms.getPreferredSize().height));
    highTermSearch.add(highWeight);
    highTermSearch.add(highTerms);

    lowTermSearch = new JPanel();
    lowTermSearch.setLayout(new BoxLayout(lowTermSearch, BoxLayout.LINE_AXIS));
    lowWeight = new JLabel("Low Weight Search Terms:");
    lowTerms = new JTextField();
    lowTerms.setMaximumSize(new Dimension(Integer.MAX_VALUE, lowTerms.getPreferredSize().height));
    lowTermSearch.add(lowWeight);
    lowTermSearch.add(lowTerms);

    // create text panes for short and long returns
    shortForm = new JList<String>();
    shortScroll = new JScrollPane(shortForm); // Comment
    shortScroll.setSize(new Dimension(300, 750)); // Comment
    shortForm.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.GRAY));

    paragraphsPanel = new JPanel();
    paragraphsPanel.setLayout(new BoxLayout(paragraphsPanel, BoxLayout.LINE_AXIS));
    paragraphsFound = new JLabel();
    paragraphsFound.setText(" ");
    paragraphsPanel.add(paragraphsFound);
    paragraphsPanel.add(Box.createHorizontalGlue());

    longForm = new JTextPane();
    longScroll = new JScrollPane(longForm); // Comment
    longScroll.setSize(new Dimension(300, 750)); // Comment
    longForm.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.GRAY));

    // create search term action
  }
}
